import React from "react";
import { DocsThemeConfig } from "nextra-theme-docs";
import styles from "./components/counters.module.css";

const config: DocsThemeConfig = {
    useNextSeoProps() {
        return {
          titleTemplate: "%s – Winning Formula | Vijay",
        };
    },
    logo: (
        <span
            className={`nx-flex nx-items-center hover:nx-invert ltr:nx-mr-auto rtl:nx-ml-auto ${styles.logo_mask}`}
        >
        Winning
        &nbsp;
            <span>Formula</span>
        </span>
    ),
    // project: {
    //     link: "https://gitlab.com/CoffeeInc/nextjs-playbook",
    // },
    // chat: {
    //   link: 'https://discord.com',
    // },
    docsRepositoryBase: "https://gitlab.com/CoffeeInc/nextjs-playbook",
    // footer: {
    //     text: (
    //         <span className="nx-items-center">
    //             Powered by
    //             <img
    //                 alt="Coffee"
    //                 style={{ height: "2.5rem", width: "auto" }}
    //                 src="https://coffeeinc.in/assets/coffee_2023_low_res.png"
    //             ></img>
    //         </span>
    //     ),
    // },
    search: {
        placeholder: "Search...",
    },
    sidebar: {
        defaultMenuCollapseLevel: 1,
    },
    feedback: {
        content: null,
    },
    editLink: {
        text: null,
    },
};

export default config;
